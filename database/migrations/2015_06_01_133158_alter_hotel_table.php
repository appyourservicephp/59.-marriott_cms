<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHotelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotel', function (Blueprint $table) {
			
			
			$table->string('country')->nullable();
                        $table->string('city')->nullable();
                        $table->integer('number_of_rooms');                       
                        $table->string('meeting_rooms')->nullable();                        
			$table->string('link')->nullable();
                        
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotel', function(Blueprint $table)
                { 
                    $table->dropColumn('country');
                    $table->dropColumn('city');
                    $table->dropColumn('number_of_rooms');                    
                    $table->dropColumn('meeting_rooms');                    
                    $table->dropColumn('link');
                });
	}

}
