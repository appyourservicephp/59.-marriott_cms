<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Upload;
use App\UploadHandler;
use App\Files;
use App\Day;
use Log;
use App\DayStats;
use App\Diagram;
use App\Hotel;
use Excel;
use Lang;
use App\Http\Requests\Admin\UploadRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Services\ImportDeleteService;
use Datatables;

class UploadController extends AdminController
{
protected $importDeleteService;
   
public function __construct(ImportDeleteService $importDeleteService)
{
    $this->importDeleteService = $importDeleteService;
}
    public function index()
    {
        // Show the page

        return view('admin.upload.index');
    }

    /**
     * Show a list of all the photo posts.
     *
     * @return View
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $hotels = Hotel::all();
        // Show the page
        return view('admin.upload.create_edit', compact('hotels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(UploadRequest $request)
    {
        
        $upload = new Upload();
        $upload->name = $request->name;
        $upload->status = '';
        $upload->hotel_id = $request->hotel;
        $upload->save();

        if ($request->file_hidden) {
            $file = Files::find($request->file_hidden);
            $file->entity_id = $upload->id;
            $file->entity_name = get_class(new Upload());
            $file->save();
        }
//        $this->convert($file->id);


        return redirect('admin/uploads')->with('success', 'Success');
    }

    public function uploadFiles()
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 60);
        $file = new Files();
        $file->save();

        $uploadHandler = new UploadHandler(array('id' => $file->id));
        $response = $uploadHandler->response;
        $uploadedFile = $response['files'][0];

        $file->name = $uploadedFile->name;
        $file->type = $uploadedFile->type;
        $file->size = $uploadedFile->size;
        $file->save();

        $input = Input::file('files');

        if ($input->getClientOriginalExtension() == 'xlsx') {
            $this->convert($file);
        }
    }

    public function convert($file)
    {

        $filePath = "public/files/" . $file->id . '/' . $file->name;
        $test = public_path() . '/files/' . $file->id . '/' . $file->name;
        $excelFile = Excel::load($filePath)->store('xls', public_path() . '/files/' . $file->id . '/', true);
        $file->name = $excelFile['file'];
        $file->save();

        if (file_exists($test)) {
            unlink($test);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id)
    {
        $item = Upload::find($id);
        $hotelName = Hotel::find($item->hotel_id);
        $file = Files::where(array('entity_id' => $item->id))->where(array('entity_name' => get_class(new Upload())))->first();
        return view('admin.upload.create_edit', array('item' => $item, 'file' => $file, 'hotelName' => $hotelName));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postEdit(UploadRequest $request, $id)
    {
        $upload = Upload::find($id);
        $upload->name = $request->name;
        $upload->status = '';
        $upload->save();

        $file = Files::where(array('entity_id' => $id))->where(array('entity_name' => get_class(new Upload())))->first();

        if (count($file) > 0) {
            if ($file->id != $request->file_hidden) {
                $this->getDeleteFiles($id);
            }
        }

        if ($request->file_hidden) {
            $file = Files::find($request->file_hidden);
            $file->entity_id = $upload->id;
            $file->entity_name = get_class(new Upload());
            $file->save();
        }

        return redirect('admin/uploads')->with('success', 'Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function getDelete($id)
    {
        $photo = Upload::find($id);
        // Show the page
        return view('admin.photo.delete', compact('photo'));
    }

    public function postDeleteImport($id)
    {
        $hotelId = Upload::find($id)->hotel_id;
        $file = Files::where(array('entity_id' => $id))->where(array('entity_name' => get_class(new Upload())))->first();
        
        if (!$file)
            return redirect('admin/uploads')->with('error', 'There is no file for import');

        $filePath = public_path() . "/files/" . $file->id . '/' . $file->name;
        if (file_exists($filePath)) {
            $path = "public/files/" . $file->id . '/' . $file->name;
            $isError = false;
            $this->importDeleteService->deleteRecords($path, $hotelId, $isError);
            if ($isError) {
                return back()->with('error', 'There is something wrong with the file. It could no be deleted');
            }
            $this->importDeleteService->deletePath($filePath, $file->id);
        }
        $file->delete();
        DayStats::where('hotel_id', $hotelId)->delete();
        $upload = Upload::find($id);
        $upload->status = '';
        $upload->save();
        return back()->with('success', Lang::get('admin/upload.file_delete'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function postDelete($id)
    {
        $hotelId = Upload::find($id)->hotel_id;
        $file = Files::where(array('entity_id' => $id))->where(array('entity_name' => get_class(new Upload())))->first();
        if ($file) {
            $filePath = public_path() . "/files/" . $file->id . '/' . $file->name;

            if (file_exists($filePath)) {
                $path = "public/files/" . $file->id . '/' . $file->name;
                $isError = false;
                $this->importDeleteService->deleteRecords($path, $hotelId, $isError);
                if ($isError) {
                    return redirect('/admin/uploads')->with('error', 'There is something wrong with the file. It could no be deleted');
                }
                $this->importDeleteService->deletePath($filePath, $file->id);                
            }
            $file->delete();
        }
        DayStats::where('hotel_id', $hotelId)->delete();
        Upload::destroy($id);
        return redirect('admin/uploads')->with('success', Lang::get('admin/upload.delete'));
    }

    public function import($id)
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 60);

        $hotelId = Upload::find($id)->hotel_id;
        $file = Files::where(array('entity_id' => $id))->where(array('entity_name' => get_class(new Upload())))->first();
        if (!$file)
            return redirect('admin/uploads')->with('error', 'There is no file for import');

        $filePath = "public/files/" . $file->id . '/' . $file->name;

        $isError = false;
        $isCopy = false;

        Excel::load($filePath, function($reader) use ($hotelId, &$isError, $id, &$isCopy) {

            $numberOfSheets = $reader->getSheetCount();

//            if ($numberOfSheets > 2) {
//                return redirect('admin/uploads')->with('error', 'Import stopped.    Number of the sheets has to be 2');
//            }
            //insert daily data into database
            $sheet = $reader->getSheet(0);
            $highestRow = $sheet->getHighestRow();

            $importDate = $sheet->getCell('B1')->getFormattedValue();
            
            if (empty($importDate)) {
                $day = $sheet->getCell('D3')->getValue();
                $day = str_pad($day, 2, '0', STR_PAD_LEFT);
                $month = $sheet->getCell('E3')->getValue();
                $month = str_pad($month, 2, '0', STR_PAD_LEFT);
                $year = $sheet->getCell('D1')->getValue();

                if (empty($day) || empty($month) || empty($year)) {
                    $isError = true;
                    $upload = Upload::find($id);
                    $upload->status = 0;
                    $upload->save();
                    return redirect('/admin/uploads')->with('error', 'Import stopped. There is something wrong with the file.');
                }
                $importDate = $day . '-' . $month . '-' . $year;
                $date = date_format(date_create($importDate), "Y-m-d");
               
            } else {
                $newDate = ($importDate - 25569) * 86400;
                $date = gmdate("Y-m-d", $newDate);
            }
            $checkFile = Upload::findDateAndHotel($hotelId, $date);
            if($checkFile) {
                $isCopy = true;
                $fileDate = Upload::find($id);
                $fileDate->status = 2;
                $fileDate->save();               
            } else {
                $fileDate = Upload::find($id);
                $fileDate->import_date = $date;
                $fileDate->save();
            }
            

            // format date
            // check if exists.            
            $days = Day::where('hotel_id', $hotelId)->where('import_date', $date)->delete();

            // check rows
            $rowM = $sheet->getCell('M' . 7)->getValue();
            $rowO = $sheet->getCell('O' . 5)->getValue();
            $rowA = $sheet->getCell('A' . 4)->getValue();


            if (!empty($rowM) || !empty($rowO) || !empty($rowA)) {
                $isError = true;
                $upload = Upload::find($id);
                $upload->status = 0;
                $upload->save();
                return redirect('/admin/uploads')->with('error', 'Import stopped. There is something wrong with the file.');
            } else {

                $rent = $sheet->getCell('B' . 2)->getValue();
                if(empty($rent)) {
                  $rent = 0;  
                }
                //daily statisticks

                for ($row = 5; $row <= $highestRow; $row++) {

                    //  Read a row of data into an array
                    $importDayData = new Day();
                    $importDayData->import_date = $date;
                    $importDayData->hotel_id = $hotelId;
                    $importDayData->code = $sheet->getCell('A' . $row)->getValue();
                    $importDayData->name = $sheet->getCell('B' . $row)->getValue();
                    $importDayData->row = $sheet->getCell('C' . $row)->getValue();
                    $importDayData->day = $sheet->getCell('D' . $row)->getValue();
                    $importDayData->mtd = $sheet->getCell('E' . $row)->getValue();
                    $importDayData->fcstmtd = $sheet->getCell('F' . $row)->getValue();
                    $importDayData->fcst = $sheet->getCell('G' . $row)->getValue();
                    $importDayData->refcst = $sheet->getCell('H' . $row)->getValue();
                    $importDayData->yesterday = $sheet->getCell('I' . $row)->getValue();
                    $importDayData->last_year = $sheet->getCell('J' . $row)->getValue();
                    
                    if(empty($sheet->getCell('K' . $row)->getValue())) {
                    $importDayData->budget = 0;
                    } else {
                    $importDayData->budget = $sheet->getCell('K' . $row)->getValue();    
                    }
                    
                    if(empty($sheet->getCell('L' . $row)->getValue())) {
                    $importDayData->budgetmtd = 0;
                    } else {
                    $importDayData->budgetmtd = $sheet->getCell('L' . $row)->getValue();    
                    }
                    
                    
                    $importDayData->rent = $rent;
                    $importDayData->save();

                    //Insert into database
                }

                // diagram
                // check if exists.            
                $diagrams = Diagram::where('hotel_id', $hotelId)->where('import_date', $date)->delete();

                for ($row = 7; $row <= 40; $row++) {


                    $importDate = $sheet->getCell('R' . $row)->getValue();
                    $newDate = ($importDate - 25569) * 86400;
                    $newDate = gmdate("d-m-Y", $newDate);

                    //  Read a row of data into an array
                    $importDayData = new Diagram();
                    $importDayData->hotel_id = $hotelId;
                    $importDayData->x_y = $sheet->getCell('Q' . $row)->getValue();
                    $importDayData->date = $newDate;
                    $importDayData->actuals = $sheet->getCell('S' . $row)->getValue();
                    $importDayData->otb = $sheet->getCell('T' . $row)->getValue();
                    $importDayData->yesterday = $sheet->getCell('U' . $row)->getValue();
                    $importDayData->pick_up = $sheet->getCell('V' . $row)->getValue();
                    $importDayData->lost = $sheet->getCell('W' . $row)->getValue();
                    $importDayData->fcst = $sheet->getCell('X' . $row)->getValue();
                    $importDayData->re_fcst = $sheet->getCell('Y' . $row)->getValue();
                    $importDayData->import_date = $date;
                    $importDayData->save();

                    //Insert into database
                }


                // STYLESHEET
//            $sheet1 = $reader->getSheet(1);
//            if ($sheet1) {
//                DayStats::where('hotel_id', $hotelId)->delete();
//
//                $lastColumn = $sheet1->getHighestColumn();
//                $lastRow    = $sheet1->getHighestRow();
//                $lastColumn++;
//                for ($column = 'D'; $column != $lastColumn; ++$column) {
//
////               echo $column . '<br/>';
//                    // date format
//                    $importDate = $sheet1->getCell($column . 4)->getValue();
//                    $newDate    = ($importDate - 25569) * 86400;
//                    $date       = gmdate("d-m-Y", $newDate);
//
//                    for ($row = 5; $row <= $lastRow; $row++) {
//
//                        if ($sheet->getCell($column . 6)->getValue() == '')
//                            break;
//
//                        $importDayStatsData              = new DayStats();
//                        $importDayStatsData->row         = $sheet1->getCell('A' . $row)->getValue();
//                        $importDayStatsData->actuals_dbd = $sheet1->getCell('B' . $row)->getValue();
//                        $importDayStatsData->name        = $sheet1->getCell('C' . $row)->getValue();
//                        $data                            = $sheet->getCell($column . $row)->getValue();
//                        if ($data == '' || !is_numeric($data)) {
//                            $importDayStatsData->value = '';
//                        } else {
//                            $importDayStatsData->value = $sheet1->getCell($column . $row)->getCalculatedValue();
//                        }
//                        Log::info($column . '-' . $row);
//                        $importDayStatsData->hotel_id = $hotelId;
//
//                        $importDayStatsData->date = $date;
//                        $importDayStatsData->save();
//                    }
//                }
//            }
//            foreach ($reader->get() as $sheet) {
//              
                //echo "<pre>";
                //print_r($rowData);
//                exit();
//                
//            }
            }
        });


        if ($isError) {
            return redirect('/admin/uploads')->with('error', 'Import stopped. There is something wrong with the file.');
        }
        if($isCopy) {
             return redirect('/admin/uploads')->with('error', 'Import stopped. There is allready file with this hotel and date.');
        }

        $upload = Upload::find($id);
        $upload->status = 1;
        $upload->save();

        return redirect('admin/uploads')->with('success', Lang::get('admin/upload.import'));
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {

        $files = Upload::select(array('id', 'name', 'status', 'hotel_id', 'import_date', 'created_at'))->orderBy('id', 'desc');


        return Datatables::of($files)
                        ->add_column('actions', '
<a href="{{{ URL::to(\'admin/upload/\' . $id . \'/import\' ) }}}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-import"></span> {{ trans("admin/modal.import") }}</a>                            
<a href="{{{ URL::to(\'admin/upload/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/upload/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger delete-file"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                    
                ')
                        ->edit_column('hotel_id', '<?php $hotel = App\Hotel::find($hotel_id); echo $hotel->name; ?>')
                        ->edit_column('status', '@if($status == "1")<i class="fa fa-check green"> imported</i> @elseif ($status == "0") <i style="color:red" class="fa fa-minus"> failed</i> @elseif ($status == "2") <i style="color:blue" class="fa fa-copy"> copy</i>  @endif')
                ->edit_column('import_date', '@if($import_date != null){{date_format(date_create($import_date), "d-m-Y")}} @endif' )        
                ->edit_column('created_at', '{{date_format(date_create($created_at), "d-m-Y")}}' )
                        ->remove_column('id')->make();
    }

}
