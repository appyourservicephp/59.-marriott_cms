<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserEditRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'name' => 'required|min:3',
            'password' => 'required|confirmed',
        ];
        
        $sendEmail = $this->request->get('sendEmail');
        $password = $this->request->get('password');
        if(empty($password) && $sendEmail != 'on') {
            unset($rules['password']);
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
