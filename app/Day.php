<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Day extends Model
{

    protected $table = 'day';

    /**
     * Deletes a xls file.
     *
     * @return bool
     */
    public function delete()
    {

        return parent::delete();
    }

    public function scopeAllDates($query, $id)
    {
        return $query->distinct()->where('hotel_id', $id)->select(['import_date'])->orderBy('import_date', 'desc')->get();
    }

    public function scopeLastDay($query, $id)
    {
        if (count($this::all()) > 0)
            return $query->where('hotel_id', $id)->orderBy('import_date', 'desc')->first()->import_date;
    }

    public function scopeSecondHighest($query, $id)
    {
        return $query->distinct()->selectRaw('import_date')->where('hotel_id', $id)
                        ->whereRaw('import_date < (SELECT MAX(import_date) FROM day)')
                        ->orderBy('import_date', 'desc')->first()->import_date;
    }

    public static function DayBefore($id, $lD)
    {
        $query = Day::distinct()->where('hotel_id', $id)
                        ->whereRaw('import_date <  "' . $lD . '"')
                        ->orderBy('import_date', 'desc')->first();
        if ($query) {
            return $query;
        }
        return null;
    }

    // CALCULATIONS
    public static function occ($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query) {
                            $query->where('code', 'ST')->orWhere('code', 'SG')->orWhere('code', 'SC');
                        })->sum($type);
    }

    public static function adr($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query) {
                            $query->where('code', 'RT')->orWhere('code', 'RG')->orWhere('code', 'RC')
                            ->orWhere('code', 'RP')->orWhere('code', 'OR'); //->orWhere('code', 'NS')->orWhere('code', 'CF');
                        })->sum($type);
    }

    public static function adrBdg($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query) {
                            $query->where('code', 'RT')->orWhere('code', 'RG')->orWhere('code', 'RC')
                            ->orWhere('code', 'RP')->orWhere('code', 'OR')->orWhere('code', 'NS')
                            ->orWhere('code', 'CF');
                        })->sum($type);
    }

    public static function totalRevenue($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query) {
                            $query->where('code', 'RT')->orWhere('code', 'RG')->orWhere('code', 'RC')->orWhere('code', 'RP')
                            ->orWhere('code', 'OR')->orWhere('code', 'NS')->orWhere('code', 'CF')->orWhere('code', 'PB')
                            ->orWhere('code', 'PL')->orWhere('code', 'PD')->orWhere('code', 'PO')->orWhere('code', 'BN')
                            ->orWhere('code', 'BB')->orWhere('code', 'BW')->orWhere('code', 'BL')->orWhere('code', 'OM')
                            ->orWhere('code', 'OL')->orWhere('code', 'OP')->orWhere('code', 'OI')->orWhere('code', 'OT')
                            ->orWhere('code', 'CB')->orWhere('code', 'CR')->orWhere('code', 'CA')->orWhere('code', 'CO');
                        })->sum($type);
    }

    public static function roomRevenue($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query2) {
                            $query2->where('code', 'RT')->orWhere('code', 'RG')->orWhere('code', 'RC')
                            ->orWhere('code', 'RP')->orWhere('code', 'OR')->orWhere('code', 'NS')->orWhere('code', 'CF');
                        })->sum($type);
    }

    public static function fBRevenue($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query) {
                            $query->where('code', 'PB')->orWhere('code', 'PL')->orWhere('code', 'PD')
                            ->orWhere('code', 'PO')->orWhere('code', 'BN')->orWhere('code', 'BB')->orWhere('code', 'BW')->orWhere('code', 'BL');
                        })->sum($type);
    }

    public static function beverage($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query) {
                            $query->where('code', 'BN')->orWhere('code', 'BB')->orWhere('code', 'BW')
                            ->orWhere('code', 'BL');
                        })->sum($type);
    }

    public static function conferenceRevenue($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query) {
                            $query->where('code', 'CB')->orWhere('code', 'CR')
                            ->orWhere('code', 'CA')->orWhere('code', 'CO');
                        })->sum($type);
    }

    public static function otherRevenue($id, $lD, $type)
    {
        return Day::where('hotel_id', $id)->where('import_date', $lD)
                        ->where(function($query) {
                            $query->where('code', 'OM')->orWhere('code', 'OL')
                            ->orWhere('code', 'OP')->orWhere('code', 'OI')->orWhere('code', 'OT');
                        })->sum($type);
    }

}
