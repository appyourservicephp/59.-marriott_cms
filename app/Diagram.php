<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagram extends Model {

    protected  $table = 'diagrams';
    public $timestamps = false;
    
    public function hotels() {
        $this->belongsTo('Hotel', 'hotel_id');
    }

}
