<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DayStats extends Model {

    protected $table = 'days_statistic';
    public $timestamps = false;

}
