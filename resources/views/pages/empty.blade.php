@extends('app')
@section('title') {{isset($hotel) ? $hotel->name : '' }} :: @parent @stop
@section('content')

<div class="row stats">
    
    @include('partials.menu')
    <div class="main-content">
    <p class="empty" >There is no data for this hotel.</p>
    </div>
</div>

@include('partials.modal')

@endsection

@section('scripts')
@parent

@endsection
@stop
