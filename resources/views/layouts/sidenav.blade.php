@extends('app')
@section('content')
    <div class="row">
        @include('partials.notifications')     
        @yield('top')
    </div>
    <div class="row">
       
    </div>
    <div class="row">
        
        <div class="col-sm-12 col-md-12 main">
            
            @yield('main')
        </div>
    </div>
@endsection
