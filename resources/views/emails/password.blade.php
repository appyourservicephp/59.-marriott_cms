<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Email</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href="https://www.google.com/fonts#UsePlace:use/Collection:Open+Sans"/>
        <style type="text/css">
            body {
                margin-bottom: 0 !important;
                padding-top: 30px !important;
                padding-bottom: 0 !important;
                color: #000;
                width: 100%;
                margin: 0 auto;
                font-family: sans-serif;
                font-size: 100%
            }
            table {
                border-collapse: collapse;
                max-width: 980px;
                display: block; 
            }
            tbody {
                max-width: 100%;
                display: block;
            }
            h1 {
                margin: 0 0 45px 0;
                font-size: 16px;
                font-weight: 800;
                color: #f1a52c; 
            }
            h2 {
                margin: 0 0 45px 0;
                font-size: 14px;
                font-weight: 500;
            }
            p {
                font-size: 14px;  font-weight: 500;
            }
            .p-margin {
                font-size: 14px;
                font-weight: 500;
                margin-top: 0;
            }
            ul {
                list-style: decimal;
                font-size: 14px;
                font-weight: 500;
            }
            a {
                color: #f1a52c;
                text-decoration: none;
            }
            .invoice {
                margin-bottom: 20px;
                font-size: 12px;
                font-weight: 500;
                margin-top:30px
            }
        </style>
    </head>
    <body>

        <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" >

            <tbody  align="left">
                <tr>
                    <td>
                        <h1>Welcome to Nordic Hospitality Performance App</h1>
                        <h2>Dear Mr/Mrs, {{$name}}</h2>
                        <p>Thank you for your interest in our Nordic Hospitality Performance App.</p>
                        <p class="p-margin">We are pleased to give you access to the application.</p>
                        <p class="p-margin">You can access the application on: <span style="color:#f1a52c;">http://flash.shmanagement.com</span></p>
                        <p style="font-size: 14px;  font-weight: 500; margin-top: 35px; margin-bottom: 0;">Your log in will be you <strong>{{$email}}</strong></p>
                        <p class="p-margin">Your password is: <strong>{{$password}}</strong></p>
                        <p style="margin:30px 0;"></p>
                    </td>
                </tr>
                <tr>
                    <td>                        
                        <p>If your device runs on iOS 9 do the following to make it possible to open the downloaded application.
                        </p>
                        <ul>
                            <li> Go into settings.</li>
                            <li> Go to general.</li>
                            <li> Press on profile.</li>
                            <li> Select App Your Service B.V.</li>
                            <li> Press on trust 'App Your Service B.V.'</li>
                        </ul>
                        <p>Please contact <a href="mailto:rosie.elzas@marriott.com">rosie.elzas@marriott.com</a>, if you have any questions.</p>
                    </td>
                </tr>
                <tr style="max-width: 100%;  margin-top: 30px">
                    <td style="max-width: 80%;" >

                        <p>Best Regards,</p>

                        <p><strong>Scandinavian Hospitality Management</strong></p>

                        <p>Hettenheuvelweg 51, 1101BM Amsterdam, The Netherlands</p>

                        <img src="{{asset('assets/site/images/logo.jpg')}}" alt="logo"/>
                        <p class="invoice"><small>This communication contains information from Marriott and Scandinavian Hospitality Management AS that may be confidential. Except for personal use by the intended recipient, or as expressly authorized by the sender, any person who receives this information is prohibited from disclosing, copying, distributing, and/or using it. If you have received this communication in error, please immediately delete it and all copies, and promptly notify the sender. Nothing in this communication is intended to operate as an electronic signature under applicable law.
                                Save a tree. Don't print this e-mail unless it's really necessary</small></p>
                    </td>
                </tr>   

            </tbody>




        </table>
    </body>
</html>
