@extends('admin.layouts.default')
{{-- Web site Title --}}
@section('title') {{{ trans("admin/hotel.hotel") }}} :: @parent @stop

@section('content')
<div class="page-header">
    <h3>
        {{{ trans("admin/hotel.new") }}}
        <div class="pull-right">
            <a href="{{{ URL::to('admin/hotels') }}}"
               class="btn btn-sm  btn-primary"><span
                    class="glyphicon glyphicon-backward"></span> {{
				trans("admin/modal.back") }}</a>
        </div>
    </h3>
</div>
<form class="form-horizontal" method="post"
      action="@if (isset($hotel)){{ URL::to('admin/hotel/' . $hotel->id . '/edit') }} @else {{ URL::to('admin/hotel/create') }}@endif"
      autocomplete="off" enctype="multipart/form-data">
    @if(isset($hotel))
    <input type="hidden" name="hid" value="1"/>
    @endif
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab-general">
            <br/>
            <!-- PICTURE -->

            <div class="col-md-12  {{{ $errors->has('image') ? 'has-error' : '' }}}">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="image">{{
						trans('admin/hotel.image') }}</label>
                    <div class="col-md-10">
                        <input id="input-1" name="image" type="file" class="file" data-show-upload="false" data-show-caption="true">
                        {!! $errors->first('image', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>
            <hr>

            <!-- NAME -->
            <div class="col-md-12 {{{ $errors->has('name') ? 'has-error' : '' }}}">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="name">{{
						trans('admin/modal.name') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" tabindex="1"
                               type="text"
                               name="name" id="name"
                               value="{{{ Input::old('name', isset($hotel) ? $hotel->name : null) }}}">
                        {!! $errors->first('name', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>

            <!-- SQR MTR -->
            <div class="col-md-12 ">
                <div class="form-group {{{ $errors->has('sqr') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="sqr">{{
						trans('admin/hotel.sqr_mtr') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" tabindex="1"
                               type="text"
                               name="sqr" id="sqr"
                               value="{{{ Input::old('sqr', isset($hotel) ? $hotel->sqr_mtr : null) }}}">
                        {!! $errors->first('sqr', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>
            <!-- CITY -->
            <div class="col-md-12 ">
                <div class="form-group {{{ $errors->has('city') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="city">{{
						trans('admin/hotel.city') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" tabindex="1"
                               type="text"
                               name="city" id="city"
                               value="{{{ Input::old('city', isset($hotel) ? $hotel->city : null) }}}">
                        {!! $errors->first('city', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>

            <!-- Country -->
            <div class="col-md-12 ">
                <div class="form-group {{{ $errors->has('country') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="country">{{
                                                            trans('admin/hotel.country') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" tabindex="1"
                               type="text"
                               readonly
                               value="{{{ Input::old('country', isset($hotel) ? $hotel->country : null) }}}">
                        {!! $errors->first('country', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>



            <!-- NUMBER OF ROOMS -->
            <div class="col-md-12 ">
                <div class="form-group {{{ $errors->has('number_of_rooms') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="number_of_rooms">{{
						trans('admin/hotel.number_of_rooms') }}</label>
                    <div class="col-md-10">
                        <select class="selectpicker" name="number_of_rooms" id="number_of_rooms">
                            @for($i = 0; $i <= 1000; $i++)
                            @if(isset($hotel) && $i == $hotel->number_of_rooms)
                            <option selected value="{{$i}}">{{$i}}</option>
                            @else
                            <option value="{{$i}}">{{$i}}</option>
                            @endif
                            @endfor
                        </select>
                        {!! $errors->first('number_of_rooms', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>

            <!-- MEETING ROOMS -->
            <div class="col-md-12 ">
                <div class="form-group {{{ $errors->has('meeting_rooms') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="meeting_rooms">{{
						trans('admin/hotel.meeting_rooms') }}</label>
                    <div class="col-md-10">
                        <select class="selectpicker" name="meeting_rooms" id="meeting_rooms">
                            @for($i = 0; $i <= 200; $i++)
                            @if(isset($hotel) && $i == $hotel->meeting_rooms)
                            <option selected value="{{$i}}">{{$i}}</option>
                            @else
                            <option value="{{$i}}">{{$i}}</option>
                            @endif
                            @endfor
                        </select>
                        {!! $errors->first('meeting_rooms', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>

            <!-- LINK -->
            <div class="col-md-12 ">
                <div class="form-group {{{ $errors->has('link') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="link">{{
						trans('admin/hotel.link') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" tabindex="1"
                               type="text"
                               name="link" id="link"
                               placeholder="http://"
                               value="{{{ Input::old('link', isset($hotel) ? $hotel->link : 'http://') }}}">
                        {!! $errors->first('link', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>

            <!-- OPENING DATE -->
            <?php isset($hotel) ? $date = date_create($hotel->opening_date) : ''; ?>
            <div class="form-group {{{ $errors->has('openingDate') ? 'has-error' : '' }}}">
                <label class="col-md-2 control-label" for="openingDate">{{trans('admin/hotel.date') }}</label>
                <div class="col-md-3 input-group date" id="openingDate">
                    <input  type='text' name="openingDate" class="form-control" value="{{isset($hotel) ? date_format($date, 'd/m/Y')  : null }}"  />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>            

                </div>
            </div>


            <!-- DESCRIPTION -->
            <div class="col-md-12">
                <div class="form-group {{{ $errors->has('description') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="description">{{ Lang::get("admin/hotel.description") }}</label>
                    <div class="col-md-10">
                        <textarea class="form-control full-width wysihtml5" name="description"
                                  value="description" rows="10">{{{ Input::old('description', isset($hotel) ? $hotel->description : null) }}}</textarea>
                        {!! $errors->first('description', '<span class="error help-block">:message</span>')!!}
                    </div>

                </div>
            </div>
        </div>  
    </div>
    <div class="form-group pull-right">
        <div class="col-md-12" >
            <a href="{{ URL::to('admin/hotels') }}" class="btn btn-sm btn-warning close_popup">
                <span class="glyphicon glyphicon-ban-circle"></span> {{
				trans("admin/modal.cancel") }}
            </a>

            <button type="submit" class="btn btn-sm btn-primary">
                <span class="glyphicon glyphicon-ok-circle"></span> 
                @if	(isset($hotel))
                {{ trans("admin/modal.edit") }}
                @else
                {{trans("admin/modal.create") }}
                @endif
            </button>
        </div>
    </div>
</form>
@stop 
@section('scripts')
<script type="text/javascript" src="{{asset('assets/admin/js/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/fileinput-preview.js')}}"></script>

<script type="text/javascript">
        @if (isset($hotel))
        $('#openingDate').datetimepicker({format: 'DD/MM/YYYY'});
        @else
//        $('#openingDate').datetimepicker({minDate: moment().add(10, 'd').toDate(), format: 'DD/MM/YYYY'});
        $('#openingDate').datetimepicker({format: 'DD/MM/YYYY'});
        @endif
</script>

<script>
            $("#input-1").fileinput({

    allowedFileTypes: ["image"],
            maxFileSize: 1500,
            @if (isset($hotel))
            initialPreview: [
                    "<img src='{{asset('/appfiles/hotel/'.$hotel->id .'/'.$hotel->image)}}' class='file-preview-image' >",
            ],
            overwriteInitial: true,
            initialCaption: "{{$hotel->image}}"
            @endif
    });</script>
<script type="text/javascript" src="{{asset('assets/admin/js/europe.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/typehead.js')}}"></script>

@stop
