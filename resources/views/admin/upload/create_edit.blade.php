@extends('admin.layouts.default')
{{-- Web site Title --}}
@section('title') {{{ trans("admin/upload.uploads") }}} :: @parent @stop
@section('content')
@include('notifications')
<link rel="stylesheet" href="{{asset('css/jquery.fileupload.css')}}">

<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">{{{
			trans('admin/modal.general') }}}</a></li>
</ul>
<form id="uploadFrm" class="form-horizontal" method="post"
      action="@if (isset($item)){{ URL::to('admin/upload/' . $item->id . '/edit') }}@endif"
      autocomplete="off">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

    <div class="tab-content">
        <div class="tab-pane active" id="tab-general">
            <br/>
            <!-- NAME -->
            <div id="name" class="col-md-12 {{{ $errors->has('name') ? 'has-error' : '' }}}">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="name">{{
						trans('admin/upload.name') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" tabindex="1"
                               placeholder="{{ trans('admin/upload.name') }}" type="text"
                               name="name" id="name"
                               value="{{{ Input::old('name', isset($item) ? $item->name : null) }}}">
                        {!! $errors->first('name', '<span class="help-block error">:message</span>')!!}
                    </div>
                </div>
            </div>

            <!-- HOTEL -->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="name">{{
						trans('admin/upload.hotel') }}</label>

                    <select class="selectpicker" name="hotel" >
                        @if(isset($hotels))
                        @foreach($hotels as $hotel)
                        <option value="{{$hotel->id}}">{{$hotel->name}}</option>
                        @endforeach
                        @else
                        <option value="{{$hotelName->id}}">{{$hotelName->name}}</option>
                        @endif                        
                    </select>

                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group {{{ $errors->has('file_hidden') ? 'has-error' : '' }}}">
                    <div class="col-md-2"></div>
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <div class="col-md-8">
                        @if(!isset($file))

                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Select file</span>
                            <!-- The file input field used as target for the file upload widget -->
                            <input id="file-hidden" type="hidden" name="file_hidden" value="<?php
                            if (isset($file) && $file) {
                                echo $file->id;
                            }
                            ?>" >

                            <input id="fileupload" type="file" name="files" >

                        </span>

                        <br>
                        <br>
                        <div class="spinner">
                            <div class="rect1"></div>
                            <div class="rect2"></div>
                            <div class="rect3"></div>
                            <div class="rect4"></div>
                            <div class="rect5"></div>
                        </div>
                        <div id="notification" role="alert"></div>
                        <!-- The global progress bar -->
                        <div id="progress" class="progress">
                            <div class="progress-bar progress-bar-success"></div>
                        </div>
                        <!-- The container for the uploaded files -->
                        @endif

                        <div id="files" class="files">
                            <?php if (isset($file) && $file): ?>

                                <input id="file-hidden" type="hidden" name="file_hidden" value="{{$file->id}}" >
                                <span class="glyphicon glyphicon-import"></span>  <?php echo $file->name; ?> |
                                <a  href="{{url('/admin/upload/'.$item->id.'/delete-file')}}" class="delete-file" ><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                            <?php endif; ?>
                        </div>
                        {!! $errors->first('file_hidden', '<span class="help-block error">:message</span>')!!}
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <a id="cancel" href="{{ URL::to('admin/uploads') }}" class="btn btn-sm btn-warning close_popup">
                <span class="glyphicon glyphicon-ban-circle"></span> {{
				trans("admin/modal.cancel") }}
            </a>

            <button id="create" type="submit" class="btn btn-sm btn-success">
                <span class="glyphicon glyphicon-ok-circle"></span> 
                @if(isset($file))
                {{ trans("admin/modal.edit") }}
                @else
                {{trans("admin/modal.create") }}
                @endif
            </button>
        </div>
    </div>
</form>
@stop

@section('scripts')

<script src="{{asset('js/vendor/jquery.ui.widget.js')}}"></script>
<script src="{{asset('js/jquery.iframe-transport.js')}}"></script>
<script src="{{asset('js/jquery.fileupload.js')}}"></script>
<script src="{{asset('assets/admin/js/deleteConf.js')}}"></script>
<script src="{{asset('assets/admin/js/bootstrap-select.js')}}"></script>
<script>
$('.selectpicker').selectpicker();
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '/admin/file-uploads';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        maxNumberOfFiles: 1,
        add: function (e, data) {
            data.context = $('#notification').text('Uploading...');
            data.submit();
            $('.fileinput-button').hide();

        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('#files').html(file.name);
            });

            $('#file-hidden').val(data.result.id);
            $('.spinner').hide();
            $('#notification').addClass('alert alert-success').html('<p><strong>Success!</strong> Upload finished.</p>');
            $('#create').removeAttr('disabled');
            $('#cancel').removeAttr('disabled');
            $('#progress').hide();
            $('.labelError').hide();
            $('.has-error').removeClass('has-error');
            $('.alert-danger').hide();
            $('#uploadFrm').on('submit', function (e) {
                if ($('#name input').val() == '') {
                    console.log('0');
                    $('#name').addClass('has-error');
                    return false;
                }
                $('#uploadFrm').submit();
            });
        },
        progressall: function (e, data) {
            $('#create').attr('disabled', 'disabled');
            $('#cancel').attr('disabled', 'disabled');
            $('.spinner').show();
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                    );

            if (progress == 100) {
//                $('#notification').addClass('alert alert-success').html('<p><strong>Success!</strong> Upload finished.</p>');

            }


        }
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>

@stop

